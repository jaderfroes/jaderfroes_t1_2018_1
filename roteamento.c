#include "roteador.h"
#include<stdio.h>
#include<stdlib.h>

void printaRotas(entrada *rotas, int num_rotas);
void converteIp(uint32_t n);
entrada *ordenaRotas(entrada *rotas, int num_rotas);
int filtragem(entrada * filtros, int num_filtros, uint32_t pacote);
int encaminhamento(entrada * rotas,int num_rotas, uint32_t pacote);

uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, 
	int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){

	int i;

	uint32_t *enlaces = calloc(num_enlaces + 1 ,sizeof(uint32_t)); //vetor com num_enlaces, + 1 para o firewall
	entrada *tabRot = ordenaRotas(rotas, num_rotas);
	

	for(i = 0; i < num_pacotes; ++i){ //percorre os pacotes
		
		if(filtragem(filtros, num_filtros, pacotes[i])){ //se o pacote foi filtrado
			enlaces[0] +=1; //incrementa a posicao do firewall
		}
		else{
			enlaces[encaminhamento(tabRot, num_rotas,pacotes[i])] +=1;
		}
	}



	return enlaces;
}

void printaRotas(entrada *rotas, int num_rotas){
	for (int i = 0; i < num_rotas; ++i) {
		printf("entrada %i: ", i);
		converteIp(rotas[i].endereco);
	}
	return;
}

void converteIp(uint32_t n){
	uint32_t aux, *ip;

	ip = calloc(4, sizeof(uint32_t)); //vetor que guarda as 4 casas do ip

		
	ip[0] = n >> 24; 

	aux = n >> 16;
	aux = aux << 24;
	ip[1] = aux >> 24;

	aux = n >> 8;
	aux = aux << 24;
	ip[2] = aux >> 24;

	aux = n;
	aux = aux << 24;
	ip[3] = aux >> 24;	

	printf("%d.%d.%d.%d\n",ip[0], ip[1], ip[2], ip[3]);

	n = 0;
	free(ip);

	return;
}

entrada *ordenaRotas(entrada *rotas, int num_rotas){

	uint32_t auxEndereco;
	char auxMask, auxEnlace;

	for (int j = 0; j < num_rotas; ++j){
		for (int i = 0; i < num_rotas-1; ++i){
			
			if(rotas[i].mascara < rotas[i+1].mascara){
				auxEndereco = rotas[i].endereco;
				auxMask = rotas[i].mascara;
				auxEnlace = rotas[i].enlace;

				rotas[i].endereco = rotas[i+1].endereco;
				rotas[i].mascara = rotas[i+1].mascara;
				rotas[i].enlace = rotas[i+1].enlace;

				rotas[i+1].endereco = auxEndereco;
				rotas[i+1].mascara = auxMask;
				rotas[i+1].enlace = auxEnlace;
			}
		}
	}

	return rotas;
}

int filtragem(entrada * filtros, int num_filtros, uint32_t pacote){ 
//passa por todos os filtros e retorna 1 se for filtrado

	int i;
	uint32_t auxPacote, auxFiltro;

	for (i = 0; i < num_filtros; ++i){
		auxPacote = pacote >> (32 - filtros[i].mascara);
		auxFiltro = filtros[i].endereco >> (32 - filtros[i].mascara);
		if(auxPacote == auxFiltro)
			return 1;
	}

	return 0;
}

int encaminhamento(entrada * rotas,int num_rotas, uint32_t pacote){
//passa por todos os elementos da tabela de roteamento e retorna o indice do enlace
	int i;
	uint32_t auxPacote, auxEnlace;

	for(i = 0; i < num_rotas; ++i)	{
		auxPacote = pacote >> (32 - rotas[i].mascara);
		auxEnlace = rotas[i].endereco >> (32 - rotas[i].mascara);
		if(auxPacote == auxEnlace)
			return rotas[i].enlace;
	}

	return 0;
}