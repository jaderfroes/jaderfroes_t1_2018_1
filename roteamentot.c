#include "roteador.h"
#include<stdio.h>
#include<stdlib.h>
#include <pthread.h>


#define SIZELIST 10
#define N_THREADS 4


typedef struct pacotes{
	uint32_t ip;
	struct pacotes *prox;
}nodo;

typedef struct infoLista{
	nodo *retira;
	nodo *insere;
	int cont;
}info;

typedef struct p{ //BUFFER PARA PRODUTORA
	info *ed; //ponteiro para informacoes da lista circular
	uint32_t idProdutora; //id da thread que coloca ip na lista
	pthread_mutex_t *mutexCont; //mutex do numero de elementos
	pthread_t produtora; //thread produtora
	uint32_t *pac;
	int numPacotes;
}memP;

typedef struct c{
	info *ed; //ponteiro para informacoes da lista circular
	uint32_t *idConsu; //id das threads consumidoras
	pthread_mutex_t *mutexConsu;//mutex das threads consumidoras
	pthread_mutex_t *mutexCont; //mutex do numero de elementos
	pthread_mutex_t *mutexEnlace; //mutex do vetor de enlace
	pthread_t *consu; //Ponteiro para vetor de thread
	entrada *fil;
	int numFil;
	uint32_t *enl;
	entrada *rot;
	int numRot;
	int numPacotes;
	int indiceThread;
}memC;


void printaRotas(entrada *rotas, int num_rotas);
void converteIp(uint32_t n);
entrada *ordenaRotas(entrada *rotas, int num_rotas); //ordena a tabela de roteamento
info *criaLista();
int filtragem(entrada * filtros, int num_filtros, uint32_t pacote);
int encaminhamento(entrada * rotas,int num_rotas, uint32_t pacote);
void *initiProdutora(void *arg);
void *initiConsu(void *arg);
int getNumElementos(info *filaPacotes, pthread_mutex_t *mutex);

uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, 
	int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){

	/*(void)rotas;
	(void)num_rotas;
	(void)pacotes;
	(void)num_pacotes;
	(void)filtros;
	(void)num_filtros;
	(void)num_enlaces;*/

	int i,j;
	
	uint32_t *enlaces = calloc(num_enlaces + 1 ,sizeof(uint32_t)); //vetor com num_enlaces, + 1 para o firewall
	info *filaPacotes = criaLista(); //cria lista circular
	entrada *tabRot = ordenaRotas(rotas, num_rotas);
	
	
	//CRIACAO DA THREAD PRODUTORA
	memP *buffer = malloc(sizeof(memP));
	buffer->ed = filaPacotes; //ponteiros para a lista circular
	buffer->idProdutora = 0; //id Da thread produtora
	buffer->pac = pacotes;
	buffer->numPacotes = num_pacotes;
	buffer->mutexCont = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(buffer->mutexCont, NULL);//referencia do mutex, e atributos
	pthread_create(&(buffer->produtora), NULL, initiProdutora, (void *)(buffer));
	

	//CRIACAO DA THREAD CONSUMIDORA
	memC *bufferC = malloc(sizeof(memC) * N_THREADS); //buffer com os dados necessarios para as consumidoras
	bufferC[0].mutexConsu = malloc(sizeof(pthread_mutex_t));
	bufferC[0].mutexEnlace = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(bufferC[0].mutexConsu, NULL);
	pthread_mutex_init(bufferC[0].mutexEnlace, NULL);
	for(i = 0; i < N_THREADS; ++i){ //criacao das threads
		bufferC[i].ed = filaPacotes; // dados para a lista circular
		bufferC[i].fil = filtros; //filtros do roteador
		bufferC[i].numFil = num_filtros;
		bufferC[i].enl = enlaces; //vetor de enlaces (resposta)
		bufferC[i].rot = tabRot; //tabela de roteamento ordenada
		bufferC[i].numRot = num_rotas;
		bufferC[i].numPacotes = num_pacotes;
		bufferC[i].consu = malloc(N_THREADS * sizeof(pthread_t));
		bufferC[i].indiceThread = i + 1;
		bufferC[i].mutexCont = buffer->mutexCont;
		bufferC[i].mutexConsu = bufferC[0].mutexConsu;
		bufferC[i].mutexEnlace = bufferC[0].mutexEnlace;
		pthread_create(bufferC[i].consu, NULL, initiConsu, (void *)(&(bufferC[i])));
	}

	pthread_join(buffer->produtora, NULL);
	for(j = 0; j < N_THREADS; ++j){
		pthread_join(*bufferC[j].consu, NULL);
	}

	return enlaces;
}

void printaRotas(entrada *rotas, int num_rotas) {
	for(int i = 0; i < num_rotas; ++i) {
		printf("entrada %i: ", i);
		converteIp(rotas[i].endereco);
	}
	return;
}

void converteIp(uint32_t n){
	uint32_t aux, *ip;

	ip = calloc(4, sizeof(uint32_t)); //vetor que guarda as 4 casas do ip

	ip[0] = n >> 24; 

	aux = n >> 16;
	aux = aux << 24;
	ip[1] = aux >> 24;

	aux = n >> 8;
	aux = aux << 24;
	ip[2] = aux >> 24;

	aux = n;
	aux = aux << 24;
	ip[3] = aux >> 24;	

	printf("%d.%d.%d.%d\n",ip[0], ip[1], ip[2], ip[3]);

	n = 0;
	free(ip);

	return;
}


entrada *ordenaRotas(entrada *rotas, int num_rotas){

	uint32_t auxEndereco;
	char auxMask, auxEnlace;

	for (int j = 0; j < num_rotas; ++j){
		for (int i = 0; i < num_rotas-1; ++i){
			
			if(rotas[i].mascara < rotas[i+1].mascara){
				auxEndereco = rotas[i].endereco;
				auxMask = rotas[i].mascara;
				auxEnlace = rotas[i].enlace;

				rotas[i].endereco = rotas[i+1].endereco;
				rotas[i].mascara = rotas[i+1].mascara;
				rotas[i].enlace = rotas[i+1].enlace;

				rotas[i+1].endereco = auxEndereco;
				rotas[i+1].mascara = auxMask;
				rotas[i+1].enlace = auxEnlace;
			}
		}
	}

	return rotas;
}

info *criaLista(){
	info *ret; //Informacoes da lista circular
	nodo *new; //auxiliar para a criacao dos nodos da lista;
	int i;

	ret = malloc(sizeof(info)); //informacoes da lista circular
	ret->cont = SIZELIST;

	nodo *cabeca = malloc(sizeof(nodo)); //cria cabeca 
	nodo *cauda  = malloc(sizeof(nodo)); //cria cauda

	ret->insere = cabeca;//local da proxima retirada da lista
	ret->retira = cabeca; //Nao ha de onde retirar

	cabeca->prox = cauda; //cabeca aponta pra cauda e vice-versa
	cauda->prox = cabeca;

	for(i = 0; i < SIZELIST - 2; ++i){ //Criando a lista(-2 pois a cabeca e cauda ja existem)
		new = malloc(sizeof(nodo));
		new->prox = cabeca->prox;
		cabeca->prox = new;
	}


	return ret;
}

int filtragem(entrada * filtros, int num_filtros, uint32_t pacote){ 
//passa por todos os filtros e retorna 1 se for filtrado

	int i;
	uint32_t auxPacote, auxFiltro;

	for (i = 0; i < num_filtros; ++i){
		auxPacote = pacote >> (32 - filtros[i].mascara);
		auxFiltro = filtros[i].endereco >> (32 - filtros[i].mascara);
		if(auxPacote == auxFiltro)
			return 1;
	}

	return 0;
}

int encaminhamento(entrada * rotas,int num_rotas, uint32_t pacote){
//passa por todos os elementos da tabela de roteamento e retorna o indice do enlace
	int i;
	uint32_t auxPacote, auxEnlace;

	for(i = 0; i < num_rotas; ++i)	{
		auxPacote = pacote >> (32 - rotas[i].mascara);
		auxEnlace = rotas[i].endereco >> (32 - rotas[i].mascara);
		if(auxPacote == auxEnlace)
			return rotas[i].enlace;
	}

	return 0;
}

int getNumElementos(info *filaPacotes, pthread_mutex_t *mutex) {
	int nElem;
	pthread_mutex_lock(mutex);
	nElem = filaPacotes->cont;
	pthread_mutex_unlock(mutex);
	return nElem;
}

int divideNumPacotes(int num_pacotes,uint32_t idThread){//decide quantos pacotes cada thread vai operar
	if(idThread == 1){// a Thread 1 fica com a divisao dos pacotes/threads + o resto da divisao
		return (num_pacotes/N_THREADS) + num_pacotes % N_THREADS;
	}
	else{//as demais ficam com a divisao dos pacotes/threads
		return num_pacotes/N_THREADS;
	}
}

void *initiProdutora(void *arg){
	memP *buffer = (memP *)(arg);
	info *filaPacotes = buffer->ed;	
	uint32_t *pacotes = buffer->pac;
	int num_pacotes = buffer->numPacotes;
	int i;
	//printf("criei a thread %i\n", buffer->idProdutora);
	
	for(i = 0; i < num_pacotes; ++i){
		if(getNumElementos(filaPacotes, buffer->mutexCont) > 0){ //se ha espaco na lista
			filaPacotes->insere->ip = pacotes[i];
			filaPacotes->insere = filaPacotes->insere->prox;//incrementa para o proximo local a inserir
			pthread_mutex_lock(buffer->mutexCont);
			filaPacotes->cont -=1;//cont--
			pthread_mutex_unlock(buffer->mutexCont);
		}
		else{//caso nao ha espaco na lista
			while(getNumElementos(filaPacotes, buffer->mutexCont) == 0);//so segue quando houver espaco na lista
			--i; //faz voltar para a posicao anterior
		}
	}

	pthread_exit(NULL);
}

void *initiConsu(void *arg){
	memC *buffer = (memC *)(arg);
	info *filaPacotes = buffer->ed;
	int num_pacotes = buffer->numPacotes;
	entrada *filtros = buffer->fil;
	int num_filtros = buffer->numFil;
	int i;
	uint32_t *enlaces = buffer->enl;
	entrada *tabRot = buffer->rot;
	int num_rotas = buffer->numRot;
	uint32_t ipAux;
	int indiceEnlace;

	//printf("criei thread %d\n", buffer->idConsu[buffer->indiceThread]);
	num_pacotes = divideNumPacotes(num_pacotes,buffer->indiceThread);

	for(i = 0; i < num_pacotes; ++i){
		pthread_mutex_lock(buffer->mutexConsu);
		if(getNumElementos(filaPacotes, buffer->mutexCont) < SIZELIST){//caso haja elemento na lista
			ipAux = filaPacotes->retira->ip; //auxiliar para permitir paralelismo no calculo do enlace
			filaPacotes->retira = filaPacotes->retira->prox;
			pthread_mutex_lock(buffer->mutexCont);
			filaPacotes->cont++;
			pthread_mutex_unlock(buffer->mutexCont);
			pthread_mutex_unlock(buffer->mutexConsu);
			if(filtragem(filtros, num_filtros, ipAux)){ //se o pacote foi filtrado
				pthread_mutex_lock(buffer->mutexEnlace);
				enlaces[0]++;//incrementa a posicao do firewall
				pthread_mutex_unlock(buffer->mutexEnlace);
			}
			else{//caso ele va para a tabela de roteamento
				indiceEnlace = encaminhamento(tabRot, num_rotas, ipAux);
				pthread_mutex_lock(buffer->mutexEnlace);
				enlaces[indiceEnlace]+=1;//incrementa a posicao do enlace calculado
				pthread_mutex_unlock(buffer->mutexEnlace);
			}
		}
		else{//caso nao haja elemento na lista
			pthread_mutex_unlock(buffer->mutexConsu);
			while(getNumElementos(filaPacotes, buffer->mutexCont) == SIZELIST);//enquanto a lista está vazia
			--i; //para garantir que a thread não perderá nenhum pacote
		}
	}
	pthread_exit(NULL);
}