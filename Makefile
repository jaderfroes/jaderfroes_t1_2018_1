CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -pthread
LDFLAGS=-lm -Wno-missing-braces

all: grade gradet

aluno: roteadort.c aluno.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o aluno roteadort.c aluno.c

test: roteamento.c test.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o test roteamento.c test.c

testt: roteamentot.c testt.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o testt roteamentot.c testt.c


grade: test
	./test

gradet: testt
	./testt

teste_aluno: aluno
	./aluno

clean:
	rm -rf *.o test testt aluno
