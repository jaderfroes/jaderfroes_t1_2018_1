#include "roteador.h"
#include "simpletest.h"
#include <assert.h>

uint32_t iptoint32(int a, int b, int c, int d){
	return (uint32_t) (a<<24)+(b<<16)+(c<<8)+d;
}

void teste1(){
	uint32_t * resultado;

	entrada rotas[] = {	{iptoint32(19,5,0,0), 16,  2},
						{iptoint32(19,5,6,0), 24,  1},
						{iptoint32(19,5,5,0), 24,  3},
						{iptoint32(19,20,1,1), 32, 2}};

	entrada filtros[] = {	{iptoint32(25,25,0,0) , 16, 0},
							{iptoint32(13,13,45,0), 24, 0},
							{iptoint32(19,5,1,0)  , 24, 0}};

	uint32_t pacotes[] = {	iptoint32(19,5,2,2),
						   	iptoint32(19,5,5,2),
							iptoint32(19,5,6,7),
							iptoint32(19,20,1,1),
							iptoint32(25,25,32,1),
							iptoint32(13,13,45,50),
							iptoint32(19,5,1,1)};

	WHEN("Um pacote para cada regra de roteamento");

	resultado = roteamento(rotas, 4, pacotes, 7, filtros, 3, 3);
	assert(resultado);

	IF("Duas regras de roteamento direcionando para o mesmo enlace");

	THEN("O enlace que tem duas regras deve receber dois pacotes");
	isEqual(resultado[2], 2, 1);

	THEN("Os outros elnaces devem receber um pacote cada");
	isEqual(resultado[1], 1, 1);
	isEqual(resultado[3], 1, 1);

	THEN("O firewall recebe três pacotes");
	isEqual(resultado[0], 3, 1);

	return;
}

void teste2(){
	uint32_t * resultado;

	entrada rotas[] = { 
						{iptoint32(192,168,0,0), 16,  2}, 
						{iptoint32(192,5,0,0), 24,  1}, 
						{iptoint32(154,12,0,0), 32,  3} 
					  };

	entrada filtros[] = {	{iptoint32(192,5,0,0) , 24, 0},
							{iptoint32(131,13,50,0), 24, 0},
							{iptoint32(19,5,1,0)  , 24, 0}
						};

	uint32_t pacotes[] = {  
							iptoint32(192,5,0,0), //0
							iptoint32(192,168,1,2), //2
							iptoint32(154,12,0,0),  //3
					    	iptoint32(192,5,0,14), //0
					    	iptoint32(192,168,21,2), //2
					    	iptoint32(154,12,0,0) //3
					    }; 

	WHEN("Cada enlace recebe dois pacotes");
	resultado = roteamento(rotas, 3, pacotes, 6, filtros, 3, 3);

	IF("Um filtro é igual a regra da tabela de roteamento");

	THEN("O enlace respectivo não recebe nenhum pacote");
	isEqual(resultado[1], 0, 1);

	THEN("Todos os outros elnaces recebem dois pacotes");
	isEqual(resultado[0], 2, 1);
	isEqual(resultado[2], 2, 1);
	isEqual(resultado[3], 2, 1);

	return;
}


void teste3(){
	uint32_t *resultado;


	entrada rotas[] = {
		{iptoint32(192,168,0,0), 16,  2}, 
		{iptoint32(192,200,0,0), 24,  1}, 
		{iptoint32(131,148,0,0), 32,  3},
		{iptoint32(19,3,0,0), 16,  4}
	};

	entrada filtros[] = {
		{iptoint32(192,5,0,0) , 24, 0},
		{iptoint32(131,13,50,0), 24, 0},
		{iptoint32(19,5,1,0)  , 24, 0}
	};

	uint32_t pacotes[] = {  
							iptoint32(192,5,0,2), //0
							iptoint32(192,200,0,1), //1
							iptoint32(192,168,14,2), //2
							iptoint32(131,148,0,0),  //3
							iptoint32(19,3,20,20), //4
					    	iptoint32(131,13,50,14), //0
					    	iptoint32(192,200,0,22), //1
					    	iptoint32(192,168,21,2), //2
					    	iptoint32(131,148,0,0), //3
					    	iptoint32(19,3,20,4), //4
					    	iptoint32(19,5,1,1), //0
					    	iptoint32(19,5,1,2), //0
					    	iptoint32(19,5,1,3) //0
					    }; 
	WHEN("Eu tenho 13 pacotes");
	resultado = roteamento(rotas, 4, pacotes, 13, filtros, 3, 4);

	THEN("Cada enlace recebe 2 pacotes e o firewall recabe 5 pacotes");
	isEqual(resultado[0], 5, 1);
	isEqual(resultado[1], 2, 1);
	isEqual(resultado[2], 2, 1);
	isEqual(resultado[3], 2, 1);
	isEqual(resultado[4], 2, 1);

}
	
int main(){

	DESCRIBE("Meus testes para trabalho 1 SO");

	teste1();
	teste2();
	teste3();
	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

}